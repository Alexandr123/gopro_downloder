Usage example:

python3 gopro_downloader.py -i 10.10.10.11:10.10.10.12:10.10.10.14:10.10.10.13 -o /home/alex/HDD/download/HW/360cam/gopro_video/

Options description:


-i  -  list of gopro ip addresses in format: xxx.xxx.xxx.xxx:xxx.xxx.xxx.xxx:...


-o  -  path to the output folder
import os, sys
import subprocess
import requests
import argparse

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--ips", type=str, required=True,
	help="list of gopro ip addresses in format: xxx.xxx.xxx.xxx:xxx.xxx.xxx.xxx:...")
ap.add_argument("-o", "--output", type=str, required=True,
	help="path to the output files")
args = vars(ap.parse_args())

dest_dir = args["output"]
gopro_ip_list = args["ips"].split(":")
processes = []

def gopro_getListFiles(ip):
    r = requests.get('http://{}:8080/gp/gpMediaList'.format(ip))
    return r.json()

def gopro_deleteAll(ip):
    requests.get('http://{}/gp/gpControl/command/storage/delete/all'.format(ip))

for gp in range(len(gopro_ip_list)):
	try:
		if dest_dir[-1] != '/':
			dest_dir = dest_dir + "/"
		os.mkdir(str(dest_dir) + str(gopro_ip_list[gp]))
		#print("mkdir: " + str(dest_dir) + str(gopro_ip_list[gp]))
	except Exception:
		pass
    jlist = gopro_getListFiles(gopro_ip_list[gp])
    gp_dir = jlist["media"][0]["d"]
    length = len(jlist["media"][0]["fs"])

    for i in range(length):
        print(jlist["media"][0]["fs"][i]["n"])
        gp_file = jlist["media"][0]["fs"][i]["n"]
        cmd = "wget -P {} http://{}:8080/videos/DCIM/{}/{}"
        cmd_f = cmd.format(dest_dir.format(gopro_ip_list[gp]), gopro_ip_list[gp], gp_dir, gp_file)
        process = subprocess.Popen(cmd_f, shell=True, stdout=subprocess.PIPE)
        process.wait()

    gopro_deleteAll(gopro_ip_list[gp])

#"""
